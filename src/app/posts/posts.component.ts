import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
    styles: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }
  `]
})
export class PostsComponent implements OnInit {
  
  isLoading:Boolean = true;
  posts;
  currentPost;

  select(post){
    this.currentPost = post;
  }

  constructor(private _postsService: PostsService) { }
  
  addPost(post){
    //this.posts.push(post);
    this._postsService.addPost(post);
  }

  deletePost(post){
    //this.posts.splice(
      //this.posts.indexOf(post),1)
    this._postsService.deletePost(post);
  }

  updatePost(post){
     //this.posts.splice(
     // this.posts.indexOf(post),1,post)
    this._postsService.updatePost(post);
  }

  ngOnInit() {
    this._postsService.getPosts().subscribe(postsData => 
    {this.posts = postsData;
      this.isLoading = false,
      console.log(this.posts)});
  }

}
