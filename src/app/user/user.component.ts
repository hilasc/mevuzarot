import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {

  user:User;
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>();
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  originalName = '';
  originalEmail = '';

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit (this.user);
  }

  toggleEdit(){
    this.originalName = this.user.name;
    this.originalEmail = this.user.email;

    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit'
    if(!this.isEdit){
      this.editEvent.emit(this.user);
    }
  }

  cancelEdit() {
    this.user.name = this.originalName;
    this.user.email = this.originalEmail;
    this.isEdit = false;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  }

  ngOnInit() {
  }

}
